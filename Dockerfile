FROM python:3.11-slim-bullseye

COPY python_reqs.txt /root/
COPY collection_reqs.yml /root/
COPY .ansible.cfg /root/

WORKDIR /root

RUN pip install -r python_reqs.txt
RUN ansible-galaxy install -r collection_reqs.yml